
//afficher Prix de la commande
document.getElementById('sommaire').style.display = "none";

function calculCommande(){
    document.getElementById('sommaire').style.display = "block";
    let sTotal = parseFloat(document.getElementById('commande').value * 10).toFixed(0);
    document.getElementById('stotal').textContent = "Sous-total: " + sTotal + "$";
    document.getElementById('taxes').textContent = "Taxes: " + parseFloat(sTotal * 0.14975).toFixed(2) + "$";
    document.getElementById('total').textContent = "Total: " + parseFloat(sTotal * 1.14975).toFixed(2) + "$";
}

document.getElementById('commande').addEventListener('input', calculCommande, false)

document.getElementById('formulaire').addEventListener('submit', valider);

//declaration des variables input du formulaire 
//evenement blur (message d'erreur qui apparait)
//evenement focus (message d'erreur disparait)
//declaration variables span (message d'erreur)
let prenom = document.getElementById('prenom');
prenom.addEventListener('blur', validerPrenom);
prenom.addEventListener('focus', clearErrorPrenom);
let errorPrenom = document.getElementById('errorPrenom');

let nom = document.getElementById('nom');
nom.addEventListener('blur', validerNom);
nom.addEventListener('focus', clearErrorNom);
let errorNom = document.getElementById('errorNom');

let adresse = document.getElementById('adresse');
adresse.addEventListener('blur', validerAdresse);
adresse.addEventListener('focus', clearErrorAdresse);
let errorAdresse = document.getElementById('errorAdresse');

let ville = document.getElementById('ville');
ville.addEventListener('blur', validerVille);
ville.addEventListener('focus', clearErrorVille);
let errorVille = document.getElementById('errorVille');

let codePostal = document.getElementById('codePostal');
codePostal.addEventListener('blur', validerCodePostal);
codePostal.addEventListener('focus', clearErrorCodePostal);
let errorCodePostal = document.getElementById('errorCodePostal');

let pays = document.getElementById('pays');
pays.addEventListener('blur', validerPays);
pays.addEventListener('focus', clearErrorPays);
let errorPays = document.getElementById('errorPays');

let commande = document.getElementById('commande');
commande.addEventListener('blur', validerCommande);
commande.addEventListener('focus', clearErrorCommande);
let errorCommande = document.getElementById('errorCommande');

let date = document.getElementById('date');
date.addEventListener('blur', validerDate);
date.addEventListener('focus', clearErrorDate);
let errorDate = document.getElementById('errorDate');

let credit = document.getElementById('credit');
credit.addEventListener('blur', validerCredit);
credit.addEventListener('focus', clearErrorCredit);
let errorCredit = document.getElementById('errorCredit');

//validation des elements du formulaire
let error = false;
function valider(e){
    error = false;
    validerPrenom();
    validerNom();
    validerAdresse();
    validerVille();
    validerCodePostal();
    validerPays()
    validerCommande()
    validerDate()
    validerCredit()
    
    //s'il y a une erreur, bloquer l'envoi du formulaire
    if (error){
      e.preventDefault();
    }
}

    function validerPrenom(){
    if (prenom.value.trim() === ""){
        document.getElementById("errorPrenom").textContent = "Veuillez entrer votre prénom";
        prenom.classList.add("rouge");
        error = true;
      }
    }
    function validerNom(){
      if (nom.value.trim() === ""){
        errorNom.textContent = "Veuillez entrer votre nom";
        nom.classList.add("rouge");
        error = true;
      }
    }
    function validerAdresse(){
      if (adresse.value.trim() === ""){
        errorAdresse.textContent = "Veuillez entrer votre adresse";
        adresse.classList.add("rouge");
        error = true;
      }
    }
    function validerVille(){
      if (ville.value.trim() === ""){
        errorVille.textContent = "Veuillez entrer votre ville";
        ville.classList.add("rouge");
        error = true;
      }
    }
    function validerCodePostal(){
      let formatCodePostal = /^[a-z][0-9][a-z][\s]*[0-9][a-z][0-9]$/i;
      if (!formatCodePostal.test(codePostal.value.trim()) || codePostal.value === ""){
        errorCodePostal.textContent = "Veuillez entrer un code postal valide";
        codePostal.classList.add("rouge");
        error = true;
      }
    }
    function validerPays(){
      if (pays.value == "choix"){
        errorPays.textContent = "Veuillez choisir votre pays";
        pays.classList.add("rouge");
        error = true;
      }
    }
    function validerCommande(){
      if (commande.value == 0){
        errorCommande.textContent = "Veuillez choisir le nombre de produits que vous voulez commander";
        commande.classList.add("rouge");
        error = true
      }
    }
    function validerDate(){
      let dateChoisie = new Date(date.value)
      let today = new Date();
      if (date.value === "" || today > dateChoisie){
        errorDate.textContent = "Veuillez choisir une date de livraison valide";
        date.classList.add("rouge");
        error = true
      }
    }
      //validation carte de credit qui commence par 4540 ou 5258 et doit avoir 16 chiffres
    function validerCredit(){
      let formatCredit = /^(4540|5258)[0-9]{12}$/;
      if (!formatCredit.test(credit.value.trim())){
        errorCredit.textContent = "Veuillez entrer un numéro de carte de crédit valide";
        credit.classList.add("rouge");
        error = true
      }    
   }

  //effacer les messages d'erreurs sur focus
  function clearErrorPrenom(){
    errorPrenom.textContent = "";
  }
  function clearErrorNom(){
    errorNom.textContent = "";
  }
  function clearErrorAdresse(){
    errorAdresse.textContent = "";
  }
  function clearErrorVille(){
    errorVille.textContent = "";
  }
  function clearErrorCodePostal(){
    errorCodePostal.textContent = "";
  }
  function clearErrorPays(){
    errorPays.textContent = "";
  }
  function clearErrorCommande(){
    errorCommande.textContent = "";
  }
  function clearErrorDate(){
    errorDate.textContent = "";
  }
  function clearErrorCredit(){
    errorCredit.textContent = "";
  }