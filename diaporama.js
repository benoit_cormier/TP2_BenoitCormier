//ecran de demmarage du quiz
let boutonDemarrer = document.getElementById('btn-demarrer')
boutonDemarrer.addEventListener('click', demarrerQuiz);

let carrousel = document.getElementById('carrousel');

let decompte = document.getElementById('decompte');
let score = document.getElementById('score');

function demarrerQuiz(){
    carrousel.style.display = 'block';
    document.getElementById('demarrer').style.display = 'none';
    // faire un decompte de 30 secondes;
    let chrono = 31;
    let compteurInterval = setInterval(compterTempsRestant, 1000);
    function compterTempsRestant(){
      chrono--;
      decompte.textContent = "Temps restant: " + chrono;
      if (chrono == 0) {
        clearInterval(compteurInterval);
        decompte.textContent = "Temps écoulé!";
        score.textContent = "Vous avez perdu!";
        document.getElementById('body').classList.add("bg-echec");
        score.classList.add("text-white");
        decompte.classList.add("text-white");
        document.getElementById('inputReponse').style.display = "none";
      }
      //arreter le decompte si 5 points sont obtenus + afficher le temps pour reussir
      if (points === 5){
        clearInterval(compteurInterval);
        decompte.textContent = "Votre temps: " + chrono + " secondes";
      }
    }
}

//carrousel
let prev = document.getElementById('prev');
let next = document.getElementById('next');
let carImg = document.getElementById('carImg');

prev.addEventListener('click', prevItem);
next.addEventListener('click', nextItem);
window.addEventListener("keydown", function (event) {
  switch (event.key) {
    case "ArrowRight":
      nextItem();
      break;
  case "ArrowLeft":
      prevItem();
      break;
  }
});

let compteur = 0;

function prevItem(){
  compteur--;
  if (compteur === -1) {
    compteur = 4;
  } 
  displayImg();
}

function nextItem(){
  compteur++;
  if (compteur === 5) {
    compteur = 0;
  } 
  displayImg();
}

let paysActuel = [{pays: 'Canada', capitale: 'ottawa', dejaRepondu: false}, 
                  {pays: 'Niger', capitale: 'niamey', dejaRepondu: false},  
                  {pays: 'Suisse', capitale: 'bern', dejaRepondu: false},   
                  {pays: 'Japon', capitale: 'tokyo', dejaRepondu: false},   
                  {pays: 'Bolivie', capitale: 'sucre', dejaRepondu: false}];

//afficher nouvelle image carrousel + pays correspondant 
let pays = document.getElementById('questionPays');
pays.textContent = paysActuel[compteur].pays;  
function displayImg() {
  carImg.src = "images/" + compteur + ".jpg";
  pays.textContent = paysActuel[compteur].pays;
  // si la reponse a deja ete donnee, afficher la bonne reponse dans un input desactive  
  if(paysActuel[compteur].dejaRepondu){
    champReponse.value = paysActuel[compteur].capitale;
    champReponse.disabled = true;
    }
  if(!paysActuel[compteur].dejaRepondu){
    champReponse.focus();
    champReponse.value = "";
    champReponse.disabled = false;
    }
}

//validation reponses
document.getElementById('soumettreReponse').addEventListener('click', calculScore);
document.getElementById('champReponse').addEventListener('keypress', calculScore);
let champReponse = document.getElementById('champReponse');

let points = 0;

function calculScore(){
  let bonneReponse = paysActuel[compteur].capitale;
  //ajouter un point si bonne reponse et que la reponse n'a pas deja ete repondue  
  if (champReponse.value.toLowerCase().trim() === bonneReponse && !paysActuel[compteur].dejaRepondu){
    points += 1;
    score.textContent = "Score: " + points;
    paysActuel[compteur].dejaRepondu = true;
    nextItem();
    // bonne reponse aux 5 questions
    if (points === 5){
      score.textContent = "Vous avez gagné!";
      //background devient vert
      document.getElementById('body').classList.add("bg-victoire");
      }
  }
}


